import torch.nn as nn
import spdnetwork.nn as nn_spd
from torchvision import models

class ConvResNet18_4thblock_rgb_slices(nn.Module):
    def __init__(self,dtype ,device):
        super(__class__,self).__init__()
        base_model = models.resnet18(pretrained=True) #INPUT: (n,ch,h,w)
        self.model_Conv = nn.Sequential(*(list(base_model.children())[:-4]))
        self.avgpooling = nn.AdaptiveAvgPool2d( output_size= (1,1))
        self.flatten1 = nn.Flatten(start_dim = 1, end_dim = -1) # flatten en (Batch, ---> Feat,  W,H)        
        self.linear1 = nn.Linear(128, 1)        
    def forward(self,x):
        x = self.model_Conv(x) #return embeddings shape: (Batchsize,sizeEmbeddings) en resnet18 (4,1000)
        x = self.avgpooling(x)
        x = self.flatten1(x)
        x = self.linear1(x)
        return x    
    
class ConvSPD_4thBlock_3BiRe_rgb_slices(nn.Module):
    def __init__(self,dtype,device):
        super(__class__,self).__init__()
        base_model = models.resnet18(pretrained=True) 
        self.model_Conv = nn.Sequential(*(list(base_model.children())[:-4])) 
        self.covariancePooling = nn_spd.CovPool()
        self.bire3_log = nn.Sequential(
                                nn_spd.BiMap(1,1,128,64,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,64,32,dtype = dtype, device = device), 
                                nn_spd.ReEig(),
                                nn_spd.BiMap(1,1,32,16,dtype = dtype, device = device),
                                nn_spd.ReEig(),            
                                nn_spd.LogEig())
        self.linear = nn.Linear(16**2,1) 
    def forward(self,x):
        x = self.model_Conv(x) 
        x = x.view(x.shape[0],x.shape[1],-1)
        x = self.covariancePooling(x) 
        x = self.bire3_log(x)
        x = x.view(x.shape[0],-1)
        x = self.linear(x)
        return x