from typing import List
import random

def get_unique_subset(
                    elements: List[str], 
                    subset_size: int, 
                    seed: int = None) -> List[str]:
    """
    Generate a subset of unique elements from the input list.

    Args:
        elements (list): List of elements to sample from.
        subset_size (int): Number of elements to include in the subset.
        seed (int, optional): Seed value for random number generation. Defaults to None.

    Returns:
        list: A sorted list containing a subset of unique elements from the input list.
    """
    subset = []
    find_unique_subset = True

    while find_unique_subset:
        if seed:
            random.seed(seed)
        subset = sorted(random.sample(elements, subset_size))

        if len(set(subset)) == subset_size:
            find_unique_subset = False

    return subset

def get_one_5fold_subset(
                    elements: List[str], 
                    seed: int) -> List[str]:
    """
    Generate a 5-fold subset of elements with a minimum of 2 samples from each class (Parkinson or control).

    Args:
        elements (list): List of elements to sample from.
        seed (int): Seed value for random number generation.

    Returns:
        list: A list containing a subset of size 5 from the input list, with no repeated elements and
              at least 2 samples from each class (Parkinson or control).
    """
    selected_parkinson = 0
    selected_control = 0

    while selected_parkinson < 2 or selected_control < 2:
        selected_parkinson = 0
        selected_control = 0
        selected_subset = []

        for element in get_unique_subset(elements = elements, subset_size=5, seed=seed):
            if "C" in element:
                selected_control += 1
            else:
                selected_parkinson += 1
            selected_subset.append(element)

        if selected_parkinson >= 2 and selected_control >= 2:
            break
        else:
            seed += 1

    return selected_subset



def generate_5fold_splits(seed: int) -> dict:
    """
    Generate 5-fold splits for cross-validation.

    Args:
        seed (int): Seed value for random number generation to ensure reproducibility.

    Returns:
        dict: A dictionary containing 5-fold splits. Keys are in the format "fold_i_test" and "fold_i_train", 
              where 'i' represents the fold number (1 to 5).
    """
    parkinson_patients = [f"P{index:02}" for index in range(13)]
    control_patients = [f"C{index:02}" for index in range(13)]
    patients = parkinson_patients + control_patients
    all_patients = patients.copy()

    folds_dict = {}
    for i in range(1,5):
        # Get test and train lists
        test_fold = get_one_5fold_subset(elements = patients,seed = seed) 

        train_fold = [patient for patient in all_patients if patient not in test_fold]
        folds_dict[f"fold_{i}_test"] = test_fold 
        folds_dict[f"fold_{i}_train"] = train_fold
        # Update patients list
        patients = [patient for patient in patients if patient not in test_fold]
        seed += 1

    folds_dict["fold_5_test"] = patients 
    folds_dict["fold_5_train"] = [patient for patient in all_patients if patient not in patients]
    
    return folds_dict
