import torch
from torch.utils import data
import numpy as np
from skimage import io
import os
from typing import List
import random
        

class rgb_slices_dataset(data.Dataset):
    # Start the configuration of the class
    def __init__(self, data_load_path, patients_names,dtype, use_augmentation=False, transform=None, return_orig = False):
        self.data_load_path    = data_load_path #path de donde se cargaran las imagenes
        self.patients_names    = patients_names #list of names ["POO","P01",...]
        self.use_augmentation  = use_augmentation 
        self.dtype             = dtype
        self.return_orig       = return_orig
        self.transform         = transform
        self.data_paths        = self.generate_data()
        print(f"Data paths: {len(self.data_paths)}")

    def generate_data(self):
        """Genera lista de paths ("informativos") para que luego sean cargados en __getitem__
            se geenran con un _x al final, _0 es original, _1,_2, ... es para 
            saber que es aumento de datos luego en el __getitem__

        Returns:
            new_data_paths (list): Lista con los paths
        """        
        if self.use_augmentation:
            new_data_paths = []
            for name in self.patients_names:
                for eye in ["0","1"]: 
                    for sample in range(5):
                        for slice in range(4):
                            for augmentation in ["0","1","2"]:                            
                                name_2_save = [self.data_load_path + name,eye,str(sample),str(slice),augmentation]
                                name_2_save = "_".join(name_2_save)
                                new_data_paths.append(name_2_save)
            return new_data_paths         
        else:
            new_data_paths = []
            for name in self.patients_names:
                for eye in ["0","1"]: 
                    for sample in range(5):
                        for slice in range(4):
                            augmentation = "0"
                            name_2_save = [self.data_load_path + name,eye,str(sample),str(slice),augmentation]
                            name_2_save = "_".join(name_2_save)
                            new_data_paths.append(name_2_save)
            return new_data_paths

    def __len__(self):
        """Give the length of the data

        Returns:
            int: Length of the train/test data with or wo augmentation.
        """        
        return len(self.data_paths)

   
    def __getitem__(self, idx):
        complete_name = self.data_paths[idx].split('/')[-1]
        name, eye, sample,n_slice, augm = complete_name.split('_')

        #select label
        if 'P' in name:
            y = 1
        else:
            y = 0

        #name to save therefore in csv. Ej: L-P00-0-0
        if eye == '0':
            name = 'L-' + name + str(sample) + str(n_slice)
        else:
            name = 'R-' + name + str(sample) + str(n_slice) 

        #loading... the slice image
        slice_img = np.array(io.imread(self.data_paths[idx][:-2] + ".png"))
        if augm == "1": #horizontal flip
            slice_img = np.fliplr(slice_img)
        elif augm == "2": #vert. flip
            slice_img = np.flipud(slice_img)
       

        if self.transform:
            # print("TRANSFORMMM!!!")
            new_slice_img = self.transform(slice_img).to(dtype = self.dtype)
        else:
            new_slice_img = torch.from_numpy(slice_img.transpose((2,0,1))) 
        if self.return_orig:
            return slice_img,new_slice_img,y,name
        else:
            return new_slice_img,y,name


def get_subset(lista,num_element,seed = False):
    """
    return sorted with diferent values
    """    
    find = True
    while find:  
        if seed:
            random.seed(seed)
            sublista = sorted(random.sample(lista,num_element))
        else:
            sublista = sorted(random.sample(lista,num_element))
        if len(set(sublista)) == num_element:
            find = False
    return sublista

def get_one5fold(lista,seed):    
    """_summary_

    Args:
        lista (list): lista de elementos
        seed (int): semilla
    Return:
        listilla (list): lista con un un subconjunto de tamaño 5 de la lista insertada, 
                         SIN elementos repetidos y mínimo 2 de cada clase (Parkinson
                         o control).
    """    
    getit = True
    while getit:
        c, p = 0, 0
        count = 0
        listilla = []
        for m in get_subset(lista, num_element = 5, seed = seed):
            if "C" in m:
                c+=1                
            else:
                p+=1
            listilla.append(m)
            count+=1
        if c==2 or p==2:
            getit = False
        else:
            seed+=1
    return listilla

def get_dict_5fold(seed): 
    """
    A partir de la lista de pacientes C00,..., C12, P00, ..., P12
    Se genera un diccionario con las particiones para cada fold, de forma que cada partición 
    tenga 5 pacientes (fold5 tiene 6 pacientes). Y en cada partición hay mínimo 2 de cada 
    clase para ser balanceados.

    Args:
        seed ([int]): [Semilla]

    Returns:
        [dict]: [diccionario]
    """    
    parkinson_patients = [f"P{index:02}" for index in range(13)]
    control_patients   = [f"C{index:02}" for index in range(13)]
    patients = parkinson_patients + control_patients
    all_patients = patients.copy()

    folds_dict = {}
    for i in range(1,5):
        fold = get_one5fold(patients,seed = seed)
        folds_dict[f"fold_{i}_test"] = fold
        folds_dict[f"fold_{i}_train"] = [i for i in all_patients if i not in fold] 
        patients = [i for i in patients if i not in fold] 
        seed+=1
    folds_dict["fold_5_test"] = patients 
    folds_dict["fold_5_train"] = [i for i in all_patients if i not in patients]
    return folds_dict


def get_unique_sorted_subset(input_list, num_elements, seed=None):
    """
    Returns a sorted list with unique values randomly sampled from the input_list.

    Args:
        input_list (list): The input list of elements.
        num_elements (int): The number of elements to sample.
        seed (int, optional): Seed for randomization. Defaults to None.

    Returns:
        list: A sorted list with unique sampled elements.
    """
    found_unique_subset = False
    while not found_unique_subset:
        if seed:
            random.seed(seed)
        sub_list = sorted(random.sample(input_list, num_elements))
        if len(set(sub_list)) == num_elements:
            found_unique_subset = True
    return sub_list

def get_balanced_5fold_subset(input_list, seed):
    """
    Generates a list with a subset of 5 unique elements from the input_list,
    ensuring at least 2 elements from each class (Parkinson or Control).

    Args:
        input_list (list): The input list of elements.
        seed (int): Seed for randomization.

    Returns:
        list: A list with 5 unique, balanced elements.
    """
    found_balanced_subset = False
    while not found_balanced_subset:
        control_count, parkinson_count = 0, 0
        subset = []
        for element in get_unique_sorted_subset(input_list, num_elements=5, seed=seed):
            if "C" in element:
                control_count += 1
            else:
                parkinson_count += 1
            subset.append(element)
        if control_count >= 2 and parkinson_count >= 2:
            found_balanced_subset = True
        else:
            seed += 1
    return subset

def get_5fold_partitions(seed):
    """
    Generates a dictionary with partitions for each fold, ensuring that each partition
    contains 5 patients (except the last fold, which may have 6) and a minimum of 2 patients
    from each class (Parkinson or Control).

    Args:
        seed (int): Seed for randomization.

    Returns:
        dict: A dictionary containing the fold partitions.
    """
    parkinson_patients = [f"P{index:02}" for index in range(13)]
    control_patients = [f"C{index:02}" for index in range(13)]
    patients = parkinson_patients + control_patients
    all_patients = patients.copy()

    folds_dict = {}
    for i in range(1, 5):
        test_fold = get_balanced_5fold_subset(patients, seed=seed)
        folds_dict[f"fold_{i}_test"] = test_fold
        folds_dict[f"fold_{i}_train"] = [p for p in all_patients if p not in test_fold]
        patients = [p for p in patients if p not in test_fold]
        seed += 1
    folds_dict["fold_5_test"] = patients
    folds_dict["fold_5_train"] = [p for p in all_patients if p not in patients]
    return folds_dict
