import time, pytz
import numpy as np
import torch
from datetime import datetime
from sklearn.metrics import accuracy_score

def get_date():
    utc_now = pytz.utc.localize(datetime.utcnow())
    pst_now = utc_now.astimezone(pytz.timezone("America/Bogota"))
    fecha = pst_now.strftime("%d/%m/%Y %H:%M:%S")
    return fecha

def writeRowPerformance(path,info,testing):
    if testing == False:
        with open(path, "a") as fd: 
            fd.write(" ".join(map(str, info)) + "\n")
    else:
        pass

def writeTrainInfo(path,info,testing):    
    if testing == False:
        with open(path, "a") as fd:  
            fd.write(info + "\n")
    else:
        pass
    
def forward(model,criterion,i_fold,i_epoch,dataloaders,phase,
            device,testing,dataset_sizes, parameters,
            names2save=None,optimizer=None,
            writer_train=None, writer_test=None, lr_scheduler=None):
    if (testing == False) and (parameters["epochs"] == i_epoch):
        accs, losses = [], []
    ep_ytrues, ep_ypreds = [], []
    running_loss, time_batchs = 0.0, 0.0       
    for i_batch, local_data in enumerate(dataloaders[phase]):
        batch_perf_info = [phase, i_fold, i_epoch, i_batch]            
        local_batch, local_labels, local_names = local_data        
        local_batch, local_labels = local_batch.to(device), local_labels.to(device)
        batch_size = local_batch.size(0)
        
        if phase == "train": optimizer.zero_grad()
            
        start_time = time.time()
        outputs = model(local_batch)
        elapsed_time = time.time() - start_time        
        loss = criterion(outputs.squeeze(),local_labels.float())
        
        if phase == "train": 
            loss.backward()
            optimizer.step()
        
        # ---- predictions and loss
        output_prob = torch.sigmoid(outputs.squeeze())
        preds = torch.round(output_prob)
        time_batchs += elapsed_time
        running_loss += loss.item() * batch_size
        ytrue_list = local_labels.tolist()
        ep_ytrues += ytrue_list
        ypred_list = preds.tolist()    
        ep_ypreds += ypred_list
        
        #---- running accuracy
        num_correct = (preds == local_labels).sum()
        running_train_acc = float(num_correct)/float(len(local_names)) #ACC
        if parameters["epochs"] == i_epoch:
            accs.append(running_train_acc)
            losses.append(loss.item() * batch_size)
        # ---- writing performance in .csv file
        if testing == False:
            if phase == "train":
                writer_train.add_scalar(f'Training Accuracy', running_train_acc, i_epoch*len(dataloaders[phase]) + i_batch)
                writer_train.add_scalar(f'Training loss', loss, i_epoch*len(dataloaders[phase]) + i_batch)
            else:
                writer_test.add_scalar(f'Testing Accuracy', running_train_acc, i_epoch*len(dataloaders[phase]) + i_batch)
                writer_test.add_scalar(f'Val loss', loss, i_epoch*len(dataloaders[phase]) + i_batch)
            for i_name, i_true, i_pred in zip(local_names, ytrue_list,output_prob.tolist()):
                info = batch_perf_info + [float("nan"), float("nan"), float("nan"), i_name, i_true, i_pred] # acc, loss, time, name, true, pred
                writeRowPerformance(path = names2save["detailedPerformance"], info = info, testing = testing)
    
    ep_acc = accuracy_score(y_true = ep_ytrues, y_pred = ep_ypreds)        
    ep_loss = running_loss / dataset_sizes[phase]
    ep_time =  time_batchs / dataset_sizes[phase]
    if testing == False:
        if phase == "train":
            if lr_scheduler is not None:
                lr_scheduler.step()
            if parameters["epochs"] == i_epoch:
                writer_train.add_hparams(
                    hparam_dict = {"lr": parameters["lr"], 
                                "batch_size": parameters["batch_size"], 
                                "lr_other": parameters["lr_others"]},
                    metric_dict = {"Accuracy": sum(accs)/len(accs), "Loss": sum(losses)/len(losses)}
                )
        else:
            if parameters["epochs"] == i_epoch:
                writer_test.add_hparams(
                    hparam_dict = {"lr": parameters["lr"], 
                                "batch_size": parameters["batch_size"], 
                                "lr_other": parameters["lr_others"]},
                    metric_dict = {"Accuracy": sum(accs)/len(accs), "Loss": sum(losses)/len(losses)}
                )
        ep_info = [phase, i_fold, i_epoch, float("nan"), ep_acc, ep_loss, ep_time,float("nan"),float("nan"),float("nan") ]
        writeRowPerformance(path = names2save["detailedPerformance"], info = ep_info, testing = testing)
             
    return model, ep_acc, ep_loss, ep_time


def validation(model,criterion,i_fold,dataloaders,device, dataset_sizes):
    ytrues, ypreds = [], []
    running_loss = 0.0
    model.eval() 
    
    time_per_sample, sizes_batch = [], []
    
    for i_batch, local_data in enumerate(dataloaders["test"]):        
        local_batch, local_labels, _ = local_data
        local_batch, local_labels = local_batch.to(device), local_labels.to(device)
        sizes_batch.append(local_batch.size(0)) 
        
        start_time = time.time()
        outputs = model(local_batch)
        time_batch = time.time() - start_time
        time_per_sample.append(time_batch)
        
        loss = criterion(outputs.squeeze(),local_labels.float())
        output_prob = torch.sigmoid(outputs.squeeze())
        preds = torch.round(output_prob)
        ytrue_list = local_labels.tolist()
        ytrues += ytrue_list
        ypred_list = preds.tolist()    
        ypreds += ypred_list  
        running_loss += loss.item() * local_batch.size(0)  
    total_time = np.array(time_per_sample).sum() 
    avg_time_per_sample = (np.array(time_per_sample)/np.array(sizes_batch)).mean()
            
    loss_epoch_test = running_loss / dataset_sizes["test"]
    acc_epoch_test = accuracy_score(ytrues,ypreds)
    
    print(f"Fold {i_fold} [initial validation] | val_loss: {loss_epoch_test:06.4f} | val_accuracy: {acc_epoch_test:05.4f} | time: {total_time:05.3f} | time/sample: {avg_time_per_sample:07.5f}")        
    return model    

def train_model(model,criterion,optimizer,i_fold,
                dataloaders,device,testing,  
                dataset_sizes,parameters, names2save=None, 
                writer_train=None, writer_test=None, lr_scheduler=None):
    
    num_epochs = parameters["epochs"]
    save_checkpoints = parameters["save_checkpoints"]
    
    for i_epoch in range(1,num_epochs+1):
        parameters["device_used"]
        """
        # -------------   TRAINING  --------------------
        """        
        model.train()  
        model, acc_train, loss_train, time_train = forward(model = model,
                                                        criterion = criterion,
                                                        i_fold = i_fold,
                                                        i_epoch = i_epoch,
                                                        dataloaders = dataloaders,
                                                        phase = "train",
                                                        device = device,
                                                        testing = testing,
                                                        dataset_sizes = dataset_sizes,
                                                        parameters = parameters,
                                                        names2save = names2save,
                                                        optimizer = optimizer,
                                                        writer_train = writer_train,
                                                        writer_test = writer_test,
                                                        lr_scheduler = lr_scheduler
                                                        )


        if (testing == False) and (save_checkpoints==True) and (i_epoch%10==0 or i_epoch == 1):     
            torch.save(model.state_dict(), names2save["models"] + f'f-{i_fold}_ep-{i_epoch:03}.pt')    

        """        
        # -------------   VALIDATION -------------------
        """

        
        model.eval()
        model, acc_test, loss_test, time_test = forward(model = model,
                                                        criterion = criterion,
                                                        i_fold = i_fold,
                                                        i_epoch = i_epoch,
                                                        dataloaders = dataloaders,
                                                        phase = "test",
                                                        device = device,
                                                        testing = testing,
                                                        dataset_sizes = dataset_sizes,
                                                        parameters = parameters,
                                                        names2save = names2save,
                                                        optimizer = optimizer,
                                                        writer_train = writer_train,
                                                        writer_test = writer_test)    
        fecha = get_date()   
        info_trining = f"{fecha} Fold {i_fold} [{i_epoch:03}/{num_epochs}] | train_loss: {loss_train:07.5f} | train_accuracy: {acc_train:05.4f} | train_time: {time_train:05.3f} | test_loss: {loss_test:07.5f} | test_accuracy: {acc_test:05.4f} | test_time: {time_test:05.3f}"
        if testing == False:
            writeTrainInfo(path = names2save["trainingVisualization"], info = info_trining, testing = testing)              
        print(info_trining)