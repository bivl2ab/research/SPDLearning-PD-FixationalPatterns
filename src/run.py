import numpy as np
import os 
import json
from datetime import datetime
import random
from sklearn.model_selection import KFold

import torch
torch.set_num_threads(1)
import torch.nn as nn
from torch.utils import data
import torchvision.transforms as T
from torch.utils.tensorboard import SummaryWriter

from spdnetwork.optimizers import MixOptimizer
import utils.models as models
from utils.train import train_model, validation, get_date, writeTrainInfo
from utils.loaders import rgb_slices_dataset, get_5fold_partitions
from utils.utils import generate_5fold_splits


#----------------------------------------------------------------
# CONFIG MODEL PARAMETERS
#----------------------------------------------------------------
testing = False
dtype = torch.double
gpu = "0"

parameters = {
            "tipo": "original",
            "data_path": 'data/ocular_fixation_slices/', 
            "output_folder": "outputs/",
            "save_checkpoints": True,            
            "seed": 2000, 
            "lr": 1e-2,
            "lr_others": 1e-4, 
            "opti_NO_riemannian": False, #True: for no-Riemannian nets. This case lr = lr_others
            "epochs": 10,
            "batch_size": 32,            
            "name_model": "ConvSPD_4thBlock_3BiRe_rgb_slices",                
            "name_save": "ConvSPD_4thBlock_3BiRe_rgb_slices", 
            "Comment": ""
            }
    

def get_data_loaders(parameters,
                    train_patients,
                    test_patients,
                    dtype):
    train_set = rgb_slices_dataset(data_load_path = parameters["data_path"],
                                    patients_names = train_patients,
                                    dtype = dtype,
                                    transform = T.Compose([
                                                    T.ToPILImage(),
                                                    T.Resize(224),
                                                    T.ToTensor(),
                                                    T.Normalize((0.485, 0.456, 0.406),(0.229, 0.224, 0.225))]) 
                                    )                                    
                            
    test_set = rgb_slices_dataset(data_load_path = parameters["data_path"],
                            patients_names = test_patients,
                            dtype = dtype,
                            transform = T.Compose([
                                            T.ToPILImage(),
                                            T.Resize(224),
                                            T.ToTensor(),
                                            T.Normalize((0.485, 0.456, 0.406),(0.229, 0.224, 0.225))]) 
                            )
                             
    train_generator = data.DataLoader(train_set, 
                                    batch_size=parameters["batch_size"], 
                                    shuffle=True,
                                    pin_memory = False)
    test_generator = data.DataLoader(test_set, 
                                    batch_size=parameters["batch_size"], 
                                    shuffle=False,
                                    pin_memory = False)  
    return train_generator, test_generator

def run_model(gpu,parameters):
    #----------------------------------------------------------------
    # Selecting the device
    #----------------------------------------------------------------
    if gpu:
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu #aca se pone nuemro de grafica libre
        parameters["device_used"] = str(gpu)
    if torch.cuda.is_available():
        device = torch.device('cuda')
        print(f'Using GPU: {torch.cuda.get_device_name()}')
        print('CUDA Visible devices:',os.getenv('CUDA_VISIBLE_DEVICES'))
    else:
        device = torch.device('cpu')
        parameters["device_used"] = "cpu"
        print("Failed to find GPU, using CPU instead.")
    
    #----------------------------------------------------------------
    # If not Testing: Create files to save.
    #----------------------------------------------------------------
    fecha = get_date()
    fecha_ts = datetime.strptime(fecha,"%d/%m/%Y %H:%M:%S").timestamp()

    if testing == False:        
        try:
            path_name_save = parameters["output_folder"] + parameters["name_save"]
            os.mkdir(path_name_save, exist_ok = True)
            # parameters["name_save"] = parameters["name_save"].replace("name_model",parameters["name_model"])            
        except:
            path_name_save = parameters["output_folder"] + parameters["name_save"][:-1] + f"_TS{fecha_ts}/"
            os.mkdir(path_name_save)

        print(f'OUTPUT FOLDER ----> {path_name_save}')
        parameters["name_save"] = path_name_save
        parameters["name_save"] = parameters["name_save"].replace("name_model",parameters["name_model"])
            
        if parameters["save_checkpoints"] == True:
            os.mkdir(parameters["name_save"] + "checkpoints_model/")
        names2save = {'detailedPerformance': parameters["name_save"] + "detailedPerformance.csv",
                    'trainingVisualization': parameters["name_save"] + "trainingVisualization.txt",
                    'parameters': parameters["name_save"] + "parameters.json",
                    'models': parameters["name_save"] + "checkpoints_model/model_"
                    } #en train.py se le agrega el .pt                                
        print(names2save)
        # ------ Probando si existen los paths
        if (
            os.path.exists(names2save["detailedPerformance"])
            or os.path.exists(names2save["trainingVisualization"])
            or os.path.exists(names2save["parameters"])
        ):
            raise ValueError("Path already exists!")

        # ------ Guardando parameters
        json.dump(parameters, open(names2save["parameters"], "w"))

        # ------ Creando los paths
        with open(names2save["detailedPerformance"], "w") as fd:
            fd.write(fecha + "\n")
        with open(names2save["trainingVisualization"], "w") as fd:
            fd.write(fecha + "\n")
    else:
        print('*'*20 + f'          THIS IS A VALIDATION RUNNING. FILES WONT BE SAVED          ' + '*'*20)

   
    #----------------------------------------------------------------
    #                       VALIDATION
    #                Samples to make validation
    #----------------------------------------------------------------

    print(parameters["data_path"])


    """
    # ---------------------------------------------------
    #               5 KFold cross validation
    # ---------------------------------------------------
    """
    folds_dict = get_5fold_partitions(seed = parameters["seed"])

    for i_fold in range(1,6):  
        train_patients = folds_dict[f"fold_{i_fold}_train"]
        test_patients = folds_dict[f"fold_{i_fold}_test"]
        if testing == False:
            writeTrainInfo(path = names2save["trainingVisualization"], 
                            info = f"Test Patients in Fold {i_fold}:"+"-".join(test_patients), 
                            testing = testing)
            name_tb = parameters["name_model"]+f"_fold{i_fold}_bs{parameters['batch_size']}_lr{parameters['lr']}_lr_others{parameters['lr_others']}"
            if os.path.exists(parameters["output_folder"]+f'runs/train/{name_tb}') == True:
                print(f"TB PATH ALREADY EXISTS!!")
                name_tb += f"_TS{fecha_ts}"
            writer_train = SummaryWriter(parameters["output_folder"]+f'runs/train/{name_tb}')
            writer_test = SummaryWriter(parameters["output_folder"]+f'runs/val/{name_tb}')

        print(f"Validando fold [{i_fold}|{test_patients}]") 

        train_generator, test_generator = get_data_loaders(parameters = parameters,
                                                                train_patients = train_patients,
                                                                test_patients = test_patients,
                                                                dtype = dtype)
        print(f'Sampler: {len(train_generator.sampler)} {len(test_generator.sampler)}')
        dataset_sizes = {"train": len(train_generator.sampler),
                        "test": len(test_generator.sampler)}
        data_loaders = {"train": train_generator,
                        "test": test_generator}

        model_architecture = parameters["name_model"]
        model = getattr(models, model_architecture)(dtype = dtype, device = device)
        model = model.to(device)
        model = model.type(dtype)
        loss_fn = nn.BCEWithLogitsLoss()    

        if parameters["opti_NO_riemannian"]:
            opti = torch.optim.Adam(model.parameters(), lr = parameters["lr_others"])
        else:
            opti = MixOptimizer(model.parameters(), 
                            lr=parameters["lr"],
                            optimizer = torch.optim.Adam,
                            lr_others = parameters["lr_others"]
                            )     
        
        
        scheduler = None
        model = validation(model = model,
                                criterion = loss_fn,
                                i_fold = i_fold,
                                dataloaders = data_loaders,
                                device = device,
                                dataset_sizes = dataset_sizes)
        
        if testing:
            train_model(model = model,
                        criterion = loss_fn,
                        optimizer = opti,
                        i_fold = i_fold,                            
                        dataloaders = data_loaders,
                        device = device,
                        testing = testing,
                        parameters = parameters,
                        dataset_sizes = dataset_sizes
                        )                        
        else:
            train_model(model = model,
                        criterion = loss_fn,
                        optimizer = opti,
                        i_fold = i_fold,
                        dataloaders = data_loaders,
                        device = device,
                        testing = testing,
                        names2save = names2save,
                        dataset_sizes = dataset_sizes,
                        parameters = parameters,
                        writer_train = writer_train,
                        writer_test = writer_test,
                        lr_scheduler = scheduler)   

if __name__== "__main__":

    modelos = ["ConvSPD_4thBlock_3BiRe_rgb_slices",
               "ConvResNet18_4thblock_rgb_slices"
               ]
    
    names = ["ConvSPD_4thBlock_3BiRe_rgb_slices",
               "ConvResNet18_4thblock_rgb_slices"]

    for base_name,name_model in zip(names,modelos):
        name_save_model = base_name+f"/"
        parameters["name_model"] = name_model
        parameters["name_save"] =  name_save_model
        if (name_model == "ConvResNet18_4thblock_rgb_slices") or (name_model == "ConvResNet18_6thblock_rgb_slices"):
            parameters["opti_NO_riemannian"] = True

        run_model(gpu = gpu, parameters = parameters)